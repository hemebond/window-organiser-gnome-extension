/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* exported init */



'use strict';


const Meta = imports.gi.Meta;
const Shell = imports.gi.Shell;
const Gdk = imports.gi.Gdk;
const Gtk = imports.gi.Gtk;
const Main = imports.ui.main;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = imports.misc.extensionUtils.getCurrentExtension();




const TOP = 1;
const RIGHT = 2;
const BOTTOM = 4;
const LEFT = 8;
const CENTER = 16;



const KeyBinds = {
	'bottom-left':   'KP_1',  // KP_1
	'bottom-center': 'KP_2',  // KP_2
	'bottom-right':  'KP_3',  // KP_3
	'center-left':   'KP_4',  // KP_4
	'center-center': 'KP_5',  // KP_5
	'center-right':  'KP_6',  // KP_6
	'top-left':      'KP_7',  // KP_7
	'top-center':    'KP_8',  // KP_8
	'top-right':     'KP_9'   // KP_9
};

const cornerSizes = [
	(w, h) => {
		return [w/3, h/2]
	},
	(w, h) => {
		return [w/2, h/2]
	},
	(w, h) => {
		return [w/3*2, h/2]
	}
];

const horizontalEdgeSizes = [
	(w, h) => {
		return [w/3, h]
	},
	(w, h) => {
		return [w/2, h]
	},
	(w, h) => {
		return [w/3*2, h]
	}
];

const verticalEdgeSizes = [
	(w, h) => {
		return [w/3, h/2]
	},
	(w, h) => {
		return [w/2, h/2]
	},
	(w, h) => {
		return [w/3*2, h/2]
	}
];

const centerSizes = [
	(w, h) => {
		return [w/3, h/3]
	},
	(w, h) => {
		return [w/2, h/2]
	},
	(w, h) => {
		return [w/3*2, h/3*2]
	},
	(w, h) => {
		return [w/3, h]
	},
	(w, h) => {
		return [w/2, h]
	},
	(w, h) => {
		return [w/3*2, h]
	}
];



function withinThreshold(a, b, threshold) {
	if (b - threshold < a && b + threshold > a) {
		return true;
	}
	return false;
}



class WindowHandler {
	constructor(placement, sizes) {
		this.placement = placement;
		this.sizes = sizes;
	}

	getWorkArea(window) {
		const monitor = window.get_monitor();
		const workspace = window.get_workspace();

		return workspace.get_work_area_for_monitor(monitor);
	}

	getMinSize(workArea) {
		return this.sizes[0](workArea.width, workArea.height);
	}

	isAligned(workArea, windowFrame) {
		if (this.placement & LEFT)
			if (windowFrame.x != workArea.x)
				return false;

		if (this.placement & RIGHT)
			if (windowFrame.x + windowFrame.width != workArea.width)
				return false;

		if (this.placement & TOP)
			if (windowFrame.y != workArea.y)
				return false;

		if (this.placement & BOTTOM)
			if (windowFrame.y + windowFrame.height != workArea.y + workArea.height)
				return false;

		if (this.placement & CENTER)
			if (
				windowFrame.x + Math.round(windowFrame.width / 2) != workArea.x + Math.round(workArea.width / 2) &&
				windowFrame.y + Math.round(windowFrame.height / 2) != workArea.y + Math.round(workArea.height / 2)
			)
				return false;

		return true;
	}

	getNextSize(workArea, windowFrame) {
		for (let i = 0; i < this.sizes.length - 1; i++) {
			const [w, h] = this.sizes[i](workArea.width, workArea.height)

			if (withinThreshold(windowFrame.width, w, 10) & withinThreshold(windowFrame.height, h, 10)) {
				return this.sizes[i+1](workArea.width, workArea.height);
			}
		}

		// default to/return to the smallest size
		// log('return min size');
		return this.getMinSize(workArea);
	}

	resize(window) {
		const windowFrame = window.get_frame_rect();
		const workArea = this.getWorkArea(window);

		let newWidth;
		let newHeight;

		if (this.isAligned(workArea, windowFrame)) {
			[newWidth, newHeight] = this.getNextSize(workArea, windowFrame);
		}
		else {
			[newWidth, newHeight] = this.getMinSize(workArea);
		}

		// window.move_windowFrame(true, workArea.width - windowFrame.width, workArea.height - windowFrame.height);
		window.move_resize_frame(
			true,
			windowFrame.x,
			windowFrame.y,
			newWidth,
			newHeight
		);
	}

	put(window) {
		let windowFrame = window.get_frame_rect();
		let workArea = this.getWorkArea(window);
		let newX;
		let newY;

		if (this.placement & CENTER) {
			newX = workArea.x + Math.round(workArea.width / 2) - Math.round(windowFrame.width / 2);
			newY = workArea.y + Math.round(workArea.height / 2) - Math.round(windowFrame.height / 2);
		}

		if (this.placement & TOP) {
			newY = workArea.y;
		}

		if (this.placement & BOTTOM) {
			newY = workArea.y + workArea.height - windowFrame.height;
		}

		if (this.placement & LEFT) {
			newX = workArea.x;
		}

		if (this.placement & RIGHT) {
			newX = workArea.x + workArea.width - windowFrame.width;
		}

		window.move_frame(
			true,
			Math.round(newX),
			Math.round(newY)
		);
	}

	process(window) {
		window.unmaximize(Meta.MaximizeFlags.BOTH);
		this.resize(window);
		this.put(window);
	}
}



class WindowOrganiserExtension {
	constructor() {
		this._settings = ExtensionUtils.getSettings();
	}

	_handle_keypress(display, window, binding) {
		let [action, vpos, hpos] = binding.get_name().split('-').slice(1);

		// window will always be null
		if (window == null) {
			window = display.focus_window;
		}

		let position;
		let sizes = centerSizes;

		if (action == 'move') {
			if (hpos == 'center' && vpos == 'center') {
				position |= CENTER;
				sizes = centerSizes;
			}
			else if (hpos == 'center') {
				position |= CENTER;
				sizes = verticalEdgeSizes;
			}
			else if (vpos == 'center') {
				position |= CENTER;
				sizes = horizontalEdgeSizes;
			}
			else {
				sizes = cornerSizes;
			}

			if (hpos == 'left') {
				position |= LEFT;
			}
			if (hpos == 'right') {
				position |= RIGHT;
			}
			if (vpos == 'top') {
				position |= TOP;
			}
			if (vpos == 'bottom') {
				position |= BOTTOM;
			}

			const windowHandler = new WindowHandler(position, sizes);
			windowHandler.process(window);
		}
	}

	_addKeybinding(keyBindName, keyBindKey, keyBindModifiers) {
		// TODO: replace this with https://github.com/GSConnect/gnome-shell-extension-gsconnect/blob/master/src/shell/keybindings.js#L53
		let shortcut = Gtk.accelerator_name(keyBindKey, keyBindModifiers);
		this._settings.set_strv(keyBindName, [shortcut]);

		//  add_keybinding(keyBindingName, extensionSettings, keyBindingFlags, Modes, handlerFunction)
		let result = Main.wm.addKeybinding(
			keyBindName,
			this._settings,
			Meta.KeyBindingFlags.IGNORE_AUTOREPEAT,
			Shell.ActionMode.NORMAL,
			this._handle_keypress.bind(this)
		);
	}

	enable() {
		// log(`enabling ${Me.metadata.name}`);

		let moveHotkeyPrefix = this._settings.get_strv('hotkey-prefix-move');
		let resizeHotkeyPrefix = this._settings.get_strv('hotkey-prefix-resize');
		let [, moveModifierKeys] = Gtk.accelerator_parse(moveHotkeyPrefix.toString());
		let [, resizeModifierKeys] = Gtk.accelerator_parse(resizeHotkeyPrefix.toString());

		for (const [keyBindName, keyBindKeyName] of Object.entries(KeyBinds)) {
			// log(`keyBindName is ${keyBindName}`);
			// log(`keyBindKeyName is ${keyBindKeyName}`);

			let keyBindKey = Gdk.keyval_from_name(keyBindKeyName);
			// log(`keyBindKey is ${keyBindKey}`);

			this._addKeybinding('hotkey-move-' + keyBindName, keyBindKey, moveModifierKeys);
			this._addKeybinding('hotkey-resize-' + keyBindName, keyBindKey, resizeModifierKeys);
		}
	}

	disable() {
		// log(`disabling ${Me.metadata.name}`);

		for (const keyBindName of Object.keys(KeyBinds)) {
			//  add_keybinding(keyBindingName, extensionSettings, keyBindingFlags, Modes, handlerFunction)
			Main.wm.removeKeybinding('hotkey-move-' + keyBindName);
			Main.wm.removeKeybinding('hotkey-resize-' + keyBindName);
		}
	}
}

function init() {
	// log(`initialising ${Me.metadata.name}`);
	return new WindowOrganiserExtension();
}
